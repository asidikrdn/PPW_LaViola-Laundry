### Sistem Informasi Laundry
#### Tugas Mata Kuliah Perancangan dan Pemrograman Web

Live Demo : 
https://project.bakulkode.web.id/laviola/

Instruksi Pemasangan :
1. Install aplikasi XAMPP pada komputer
2. Pindahkan file zip ke folder `C:/xampp/htdocs/`
3. Extract file zip
4. Buka browser dan masuk ke phpmyadmin dengan mengakses url `localhost/phpmyadmin` pada browser
5. Buat database baru dengan nama `db_laviola` lalu masuk ke tab `Import`
6. Klik kolom `Choose File` lalu masuk ke dalam folder `PPW_LaViola-Laundry` yang tadi sudah di extract, masuk ke folder `DATABASE/Backup` dan pilih `db_laviola.sql`
7. Klik tombol `import`
8. Setelah berhasil mengimpor database, aplikasi siap digunakan. Coba buka dengan mengakses url `localhost/PPW_LaViola-Laundry` pada browser


Informasi Login Aplikasi :

ADMIN 
- username	= `Admin`
- password	= `admin`

KARYAWAN
- username	= `Kar`
- password	= `kar`


Fitur Utama
- Tampilan Dashboard
- Tampilan Daftar Layanan dan Paket yang tersedia
- Tampilan Daftar Pelanggan
- Tampilan Daftar Transaksi
- Tampilan Daftar Petugas
- Tampilan Detail Toko
- Tombol Logout

Fitur User Karyawan :
- Tambah Pelanggan
- Edit Data Pelanggan
- Tambah Transaksi
- Cetak Invoice Transaksi
- Logout

Fitur User Admin :
- Tambah Pelanggan
- Edit Data Pelanggan
- Tambah Layanan
- Edit Data Layanan
- Tambah Transaksi
- Cetak Invoice Transaksi
- Hapus Transaksi
- Edit Detail Toko
- Edit Detail Petugas
- Logout

Stack : HTML / CSS / Bootstrap 5 / JS / PHP / MySQL



[<img src='https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/github.svg' alt='github' height='40'>](https://github.com/asidikrdn)  [<img src='https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/linkedin.svg' alt='linkedin' height='40'>](https://www.linkedin.com/in/asidikrdn/)  [<img src='https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/instagram.svg' alt='instagram' height='40'>](https://www.instagram.com/asidikrdn/)    [<img src='https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/gitlab.svg' alt='gitlab' height='40'>](https://www.gitlab.com/asidikrdn/)  [<img src='https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/netlify.svg' alt='netlify' height='40'>](https://asidikrdn.netlify.app/)  

