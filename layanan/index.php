<?php
// Autentifikasi Session
session_start();
if (!isset($_SESSION['login'])) {
  header('Location: ../login');
  exit;
}

require '../functions.php';

$layanan = query('SELECT * FROM tbl_layanan');
$paket = query('SELECT * FROM tbl_paket');
$jenis_barang = query('SELECT * FROM tbl_jenis_barang');

$userlogin = $_SESSION['login'];
$isUserAdmin = query("SELECT tbl_petugas.role FROM tbl_petugas WHERE tbl_petugas.id_petugas = '$userlogin'");

// Mengaktifkan class actiive pada sidebar
$layananNavStatus = "active";
?>
<!DOCTYPE html>
<html lang="en">
<!-- Head -->
<?php include '../partial/head.php'; ?>

<body>
  <div class="container-fluid">
    <!-- Sidebar Web Version-->
    <?php include '../partial/sidebar.php'; ?>
    <!-- Header -->
    <?php include '../partial/header.php'; ?>
    <div class="row">
      <div class="col-md-10 offset-md-2 p-0">
        <div class="container-fluid p-0">
          <!-- Main Content -->
          <main id="kontenUtama">
            <div class="row g-0">
              <div class="col-12">
                <div class="px-2 d-flex flex-row justify-content-between bg-light">
                  <h4>Layanan</h4>
                  <!-- <h4>Tanggal : xx - xx - xxxx</h4> -->
                </div>
              </div>
            </div>

            <!-- Tabel Transaksi -->
            <div class="row g-0">
              <div class="col-12 col-md-4 pt-3 pb-0 px-2">
                <?php if ($isUserAdmin[0]['role'] == 'admin') { ?>
                  <a href="tambah_layanan.php" class="btn btn-primary ms-0 my-2"><i class="bi bi-plus-circle"></i> Layanan</a>
                <?php }; ?>
              </div>
            </div>
            <div class="row g-0">
              <div class="col-12 col-md-4 py-0 px-2">
                <?php if ($isUserAdmin[0]['role'] == 'admin') { ?>
                  <!-- <a href="" class="btn btn-primary ms-0 my-2">Edit Layanan</a> -->
                <?php }; ?>
                <table class="mx-auto table bg-white">
                  <tr class="table-danger">
                    <th class="text-center">Nama Layanan</th>
                    <th>Harga</th>
                    <th></th>
                  </tr>
                  <?php foreach ($layanan as $lynn) : ?>
                    <tr>
                      <td class="text-center"><?= $lynn['nama_layanan']; ?></td>
                      <td><?= rupiah($lynn['harga']); ?></td>
                      <?php if ($isUserAdmin[0]['role'] == 'admin') { ?>
                        <td>
                          <a href="edit_layanan.php?id_lynn=<?php echo $lynn['id_layanan']; ?>" class="btn btn-warning"><i class="bi bi-pencil"></i></a>
                        </td>
                      <?php } ?>
                    </tr>
                  <?php endforeach; ?>
                </table>
              </div>

              <div class="col-12 col-md-5 py-0 px-2">
                <?php if ($isUserAdmin[0]['role'] == 'admin') { ?>
                  <!-- <a href="" class="btn btn-primary ms-0 my-2 overlay-50"><i class="bi bi-plus-circle"></i> Paket</a> -->
                  <!-- <a href="" class="btn btn-primary ms-0 my-2">Edit Paket</a> -->
                <?php }; ?>
                <table class="mx-auto table bg-white">
                  <tr class="table-danger text-center">
                    <th>Nama Paket</th>
                    <th>Penyesuaian Harga</th>
                    <th>Waktu Proses</th>
                  </tr>
                  <?php foreach ($paket as $pkt) : ?>
                    <tr class="text-center">
                      <td><?= $pkt['nama_paket']; ?></td>
                      <td><?= $pkt['multiplier'] . "x Harga"; ?></td>
                      <td><?= $pkt['durasi']; ?></td>
                    </tr>
                  <?php endforeach; ?>
                </table>
              </div>

              <div class="col-12 col-md-3 py-0 px-2">
                <?php if ($isUserAdmin[0]['role'] == 'admin') { ?>
                  <!-- <a href="" class="btn btn-primary ms-0 my-2"><i class="bi bi-plus-circle"></i> Barang</a> -->
                  <!-- <a href="" class="btn btn-primary ms-0 my-2">Edit Barang</a> -->
                <?php }; ?>
                <table class="mx-auto table bg-white">
                  <tr class="table-danger text-center">
                    <th>Jenis Barang</th>
                  </tr>
                  <?php foreach ($jenis_barang as $brng) : ?>
                    <tr class="text-center">
                      <td><?= $brng['nama_barang']; ?></td>
                    </tr>
                  <?php endforeach; ?>
                </table>
              </div>
            </div>

          </main>
        </div>
      </div>
    </div>
  </div>
</body>
<script src="../assets/js/bootstrap.bundle.min.js"></script>

</html>