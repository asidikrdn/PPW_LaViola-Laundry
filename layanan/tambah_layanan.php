<?php
// Autentifikasi Session
session_start();
if (!isset($_SESSION['login'])) {
  header('Location: ../login');
  exit;
}

require '../functions.php';

if (isset($_POST['tambah_layanan'])) {
  if (tambah_layanan($_POST) > 0) {
    echo "
        <script>
            alert('Layanan berhasil ditambahkan !');
            document.location.href = 'index.php';
        </script>
    ";
  } else {
    echo "
        <script>
            alert('Layanan gagal ditambahkan !');
            document.location.href = 'index.php';
        </script>
    ";
  }
}

// Melihat id_barang
$cek_id = query("SELECT * FROM tbl_jenis_barang");
// var_dump($cek_id);
?>

<!DOCTYPE html>
<html lang="en">
<!-- Head -->
<?php include '../partial/head.php'; ?>

<body>
  <div class="container-fluid">
    <!-- Sidebar Web Version-->
    <?php include '../partial/sidebar.php'; ?>
    <!-- Header -->
    <?php include '../partial/header.php'; ?>
    <div class="row">
      <div class="col-md-10 offset-md-2 p-0">
        <div class="container-fluid p-0">
          <!-- Main Content -->
          <main id="kontenUtama">
            <div class="row g-0">
              <div class="col-12">
                <div class="px-2 d-flex flex-row justify-content-between bg-light">
                  <h4>Form Tambah Layanan</h4>
                  <!-- <h4>Tanggal : xx - xx - xxxx</h4> -->
                  <a href="index.php" class="btn btn-secondary my-1">Kembali</a>
                </div>
              </div>
            </div>

            <!-- Form -->
            <div class="row g-0">
              <div class="col-12 p-3">
                <div class="container-fluid">
                  <div class="d-flex flex-row justify-content-center align-items-center" style="height: 100vh;">
                    <div class="div">
                      <form action="" method="post">
                        <label for="nama_layanan">Nama Layanan :</label>
                        <input type="text" name="nama_layanan" id="nama_layanan" maxlength="50" required class="form-control">
                        <br>
                        <label for="id_barang">Jenis Barang :</label>
                        <select name="id_barang" id="id_barang" required class="form-control">
                          <option>-- Pilih Barang --</option>
                          <?php foreach ($cek_id as $barang) : ?>
                            <option value="<?= $barang['id_barang']; ?>">
                              <?= $barang['nama_barang']; ?>
                            </option>
                          <?php endforeach; ?>
                        </select>
                        <br>
                        <label for="harga">Harga :</label>
                        <input type="text" name="harga" id="harga" maxlength="11" required class="form-control">
                        <br>
                        <button type="submit" name="tambah_layanan" id="tambah_layanan" class="btn btn-primary">Tambah Layanan</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </main>
        </div>
      </div>
    </div>
  </div>
</body>
<script src="../assets/js/bootstrap.bundle.min.js"></script>

</html>