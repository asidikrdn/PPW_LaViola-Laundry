<?php
// Autentifikasi Session
session_start();
if (!isset($_SESSION['login'])) {
  header('Location: ../login');
  exit;
}

require '../functions.php';


$petugas = query("SELECT * FROM tbl_petugas");

$userlogin = $_SESSION['login'];
$isUserAdmin = query("SELECT * FROM tbl_petugas WHERE tbl_petugas.id_petugas = '$userlogin'");

?>
<!DOCTYPE html>
<html lang="en">
<!-- Head -->
<?php include '../partial/head.php'; ?>

<body>
  <div class="container-fluid">
    <!-- Sidebar Web Version-->
    <?php include '../partial/sidebar.php'; ?>
    <!-- Header -->
    <?php include '../partial/header.php'; ?>
    <div class="row">
      <div class="col-md-10 offset-md-2 p-0">
        <div class="container-fluid p-0">
          <!-- Main Content -->
          <main id="kontenUtama">
            <div class="row g-0">
              <div class="col-12">
                <div class="px-2 d-flex flex-row justify-content-between bg-light">
                  <h4>Petugas</h4>
                  <!-- <h4>Tanggal : xx - xx - xxxx</h4> -->
                </div>
              </div>
            </div>

            <!-- Tabel Transaksi -->
            <div class="row g-0">
              <div class="col-12 p-3">
                <?php if ($isUserAdmin[0]['role'] == 'admin') { ?>
                  <a href="register.php" class="btn btn-primary ms-0 my-2"><i class="bi bi-plus-circle"></i> Petugas</a>
                  <!-- <a href="" class="btn btn-warning ms-0 my-2">Edit Petugas</a> -->
                <?php }; ?>
                <table class="mx-auto table bg-white">
                  <tr class="table-danger text-center">
                    <th>ID Petugas</th>
                    <th>Nama Petugas</th>
                    <th>Username</th>
                    <th>Role</th>
                    <?php if ($isUserAdmin[0]['role'] == 'admin') { ?>
                      <th></th>
                    <?php }; ?>
                  </tr>
                  <?php foreach ($petugas as $crew) : ?>
                    <tr class="text-center">
                      <td><?= $crew['id_petugas'] ?></td>
                      <td><?= $crew['nama_petugas'] ?></td>
                      <td><?= $crew['username'] ?></td>
                      <td><?= $crew['role'] ?></td>
                      <?php if ($isUserAdmin[0]['role'] == 'admin') { ?>
                        <?php if ($crew['role'] == 'karyawan' || $crew['id_petugas'] == $isUserAdmin[0]['id_petugas']) { ?>
                          <td>
                            <a href="edit_petugas.php?id_petugas=<?php echo $crew['id_petugas']; ?>" class="btn btn-warning"><i class="bi bi-pencil"></i></a>
                          </td>
                        <?php }; ?>
                      <?php }; ?>
                    </tr>
                  <?php endforeach; ?>
                </table>
              </div>
            </div>

          </main>
        </div>
      </div>
    </div>
  </div>
</body>
<script src="../assets/js/bootstrap.bundle.min.js"></script>

</html>