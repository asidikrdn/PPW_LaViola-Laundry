<?php
// Autentifikasi Session
session_start();
if (!isset($_SESSION['login'])) {
  header('Location: ../login');
  exit;
}


require '../functions.php';

if (isset($_POST['update_petugas'])) {
  if (update_petugas($_POST) > 0) {
    echo "
                <script>
                    alert('DataPetugas berhasil diubah');
                </script>
            ";
  } else {
    echo mysqli_error($conn);
  }
}

$id_petugas = $_GET["id_petugas"];
$petugas = query("SELECT * FROM tbl_petugas WHERE id_petugas = '$id_petugas'");
?>

<!DOCTYPE html>
<html lang="en">
<!-- Head -->
<?php include '../partial/head.php'; ?>

<body>
  <div class="container-fluid">
    <!-- Sidebar Web Version-->
    <?php include '../partial/sidebar.php'; ?>
    <!-- Header -->
    <?php include '../partial/header.php'; ?>
    <div class="row">
      <div class="col-md-10 offset-md-2 p-0">
        <div class="container-fluid p-0">
          <!-- Main Content -->
          <main id="kontenUtama">
            <div class="row g-0">
              <div class="col-12">
                <div class="px-2 d-flex flex-row justify-content-between bg-light">
                  <h4>Form Ubah Data Petugas</h4>
                  <!-- <h4>Tanggal : xx - xx - xxxx</h4> -->
                  <a href="index.php" class="btn btn-secondary my-1">Kembali</a>
                </div>
              </div>
            </div>

            <!-- Form -->
            <div class="row g-0">
              <div class="col-12 p-3">
                <div class="container-fluid">
                  <div class="d-flex flex-row justify-content-center align-items-center" style="height: 100vh;">
                    <div class="div">
                      <form action="" method="post">
                        <label for="nama_petugas">Nama Petugas :</label>
                        <input type="text" name="nama_petugas" id="nama_petugas" maxlength="30" required class="form-control" value="<?php echo $petugas[0]['nama_petugas']; ?>">
                        <br>
                        <!-- <label for="username">Username :</label>
                        <input type="text" name="username" id="username" maxlength="10" required class="form-control" value="<?php echo $petugas[0]['username']; ?>">
                        <br> -->
                        <label for="role">Role :</label>
                        <select name="role" id="role" required class="form-control">
                          <option value="admin" <?php if ($petugas[0]['role'] == 'admin') { ?> selected <?php }; ?> >Administrator</option>
                          <option value="karyawan" <?php if ($petugas[0]['role'] == 'karyawan') { ?> selected <?php }; ?>>Karyawan</option>
                        </select>
                        <br>
                        <button type="submit" name="update_petugas" id="update_petugas" class="btn btn-primary">Update User</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </main>
        </div>
      </div>
    </div>
  </div>
</body>
<script src="../assets/js/bootstrap.bundle.min.js"></script>

</html>
