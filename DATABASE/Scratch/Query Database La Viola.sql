CREATE DATABASE db_laviola;

USE db_laviola;

CREATE TABLE tbl_petugas (
  id_petugas varchar(8),
  nama_petugas varchar(30),
  username varchar(10),
  password varchar(255),
  role varchar(10),
  CONSTRAINT PKPetugas PRIMARY KEY (id_petugas)
);


CREATE TABLE tbl_pelanggan (
  id_pel varchar(8),
  nama_pel varchar(30),
  alamat varchar(50),
  no_telp varchar(13),
  CONSTRAINT PKPelanggan PRIMARY KEY (id_pel)
);


CREATE TABLE tbl_jenis_barang (
  id_barang varchar(5),
  nama_barang varchar(10),
  CONSTRAINT PKJenisBarang PRIMARY KEY (id_barang)
);


CREATE TABLE tbl_paket (
  id_paket varchar(3),
  nama_paket varchar(30),
  multiplier float(5),
  prioritas int,
  durasi varchar(15),
  CONSTRAINT PKPaket PRIMARY KEY (id_paket)
);


CREATE TABLE tbl_layanan (
  id_layanan varchar(5),
  nama_layanan varchar(50),
  id_barang varchar(5), FOREIGN KEY (id_barang) REFERENCES tbl_jenis_barang(id_barang),
  harga int,
  CONSTRAINT PKLayanan PRIMARY KEY (id_layanan)
);


CREATE TABLE tbl_status (
	id_status varchar(3),
  keterangan varchar(15),
  CONSTRAINT PKStatus PRIMARY KEY (id_status)
);


CREATE TABLE tbl_transaksi (
  id_transaksi varchar(8),
  id_pel varchar(8), FOREIGN KEY (id_pel) REFERENCES tbl_pelanggan(id_pel),
  id_layanan varchar(5), FOREIGN KEY (id_layanan) REFERENCES tbl_layanan(id_layanan),
  id_paket varchar(3), FOREIGN KEY (id_paket) REFERENCES tbl_paket(id_paket),
  qty int,
  tgl_masuk date,
  id_status varchar(3), FOREIGN KEY (id_status) REFERENCES tbl_status(id_status),
  id_petugas varchar(8), FOREIGN KEY (id_petugas) REFERENCES tbl_petugas(id_petugas),
  CONSTRAINT PKTransaksi PRIMARY KEY (id_transaksi)
);

CREATE TABLE tbl_id_toko (
  nama_toko varchar(30),
  telp_toko varchar(15),
  alamat_toko varchar(100),
  CONSTRAINT PKIdToko PRIMARY KEY (nama_toko)
);



-- Mengisi tabel identitas toko
INSERT INTO `tbl_id_toko`(`nama_toko`,`telp_toko`,`alamat_toko`) 
VALUES ('La Viola Laundry','(021) 3546897','Jl. Sholeh Iskandar, RT.01/RW.10, Kedungbadak, Kec. Tanah Sereal, Kota Bogor, Jawa Barat 16162');



-- Mengisi tabel status
INSERT INTO `tbl_status`(`id_status`, `keterangan`) VALUES ('S01','Baru');
INSERT INTO `tbl_status`(`id_status`, `keterangan`) VALUES ('S02','Dalam Proses');
INSERT INTO `tbl_status`(`id_status`, `keterangan`) VALUES ('S03','Selesai');
INSERT INTO `tbl_status`(`id_status`, `keterangan`) VALUES ('S04','Diambil');


-- Mengisi tabel jenis barang
INSERT INTO `tbl_jenis_barang`(`id_barang`, `nama_barang`) VALUES ('B-001','Kiloan');
INSERT INTO `tbl_jenis_barang`(`id_barang`, `nama_barang`) VALUES ('B-002','Pakaian');
INSERT INTO `tbl_jenis_barang`(`id_barang`, `nama_barang`) VALUES ('B-003','Jaket');
INSERT INTO `tbl_jenis_barang`(`id_barang`, `nama_barang`) VALUES ('B-004','Bed Cover');
INSERT INTO `tbl_jenis_barang`(`id_barang`, `nama_barang`) VALUES ('B-005','Karpet');


-- Mengisi tabel paket
INSERT INTO `tbl_paket`(`id_paket`, `nama_paket`, `multiplier`, `prioritas`, `durasi`) VALUES ('P01','REGULER','1','4','3 Hari');
INSERT INTO `tbl_paket`(`id_paket`, `nama_paket`, `multiplier`, `prioritas`, `durasi`) VALUES ('P02','EXPRESS','1.5','3','2 Hari');
INSERT INTO `tbl_paket`(`id_paket`, `nama_paket`, `multiplier`, `prioritas`, `durasi`) VALUES ('P03','VIP','2','2','1 Hari');
INSERT INTO `tbl_paket`(`id_paket`, `nama_paket`, `multiplier`, `prioritas`, `durasi`) VALUES ('P04','SUPER VIP','2.5','1','6 Jam');


-- Mengisi tabel layanan
INSERT INTO `tbl_layanan` (`id_layanan`, `nama_layanan`, `id_barang`, `harga`) VALUES
('LV001', 'Cuci (Kiloan)', 'B-001', 8000),
('LV002', 'Setrika (Kiloan)', 'B-001', 8000),
('LV003', 'Cuci + Setrika (Kiloan)', 'B-001', 15000),
('LV004', 'Laundry Satuan (Pakaian)', 'B-002', 5000),
('LV005', 'Laundry Satuan (Jaket)', 'B-003', 7000),
('LV006', 'Laundry Satuan (Bed Cover)', 'B-004', 10000),
('LV007', 'Laundry Satuan (Karpet)', 'B-005', 12500);


-- Tabel Admin Sementara
INSERT INTO `tbl_petugas` (`id_petugas`, `nama_petugas`, `username`, `password`, `role`) VALUES
('crew-001', 'Administrator', 'Admin', '$2y$10$qzBpP2s6KKYVK82zxklETup6sZUfY6xQgTmdUeajYaQo8EMbfrQKC', 'admin');
INSERT INTO `tbl_petugas` (`id_petugas`, `nama_petugas`, `username`, `password`, `role`) VALUES
('crew-002', 'Karyawan', 'Kar', '$2y$10$oWe0Gnod2JZAd3sJSGVATe7OFVN8JaYyXHDcuXAxCzO87CSX/rLY2', 'karyawan');
