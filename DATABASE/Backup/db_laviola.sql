-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 18, 2022 at 10:51 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_laviola`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_id_toko`
--

CREATE TABLE `tbl_id_toko` (
  `nama_toko` varchar(30) NOT NULL,
  `telp_toko` varchar(15) DEFAULT NULL,
  `alamat_toko` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_id_toko`
--

INSERT INTO `tbl_id_toko` (`nama_toko`, `telp_toko`, `alamat_toko`) VALUES
('La Viola Laundry', '(021) 3546897', 'Jl. Sholeh Iskandar, RT.01/RW.10, Kedungbadak, Kec. Tanah Sereal, Kota Bogor, Jawa Barat 16162');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jenis_barang`
--

CREATE TABLE `tbl_jenis_barang` (
  `id_barang` varchar(5) NOT NULL,
  `nama_barang` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_jenis_barang`
--

INSERT INTO `tbl_jenis_barang` (`id_barang`, `nama_barang`) VALUES
('B-001', 'Kiloan'),
('B-002', 'Pakaian'),
('B-003', 'Jaket'),
('B-004', 'Bed Cover'),
('B-005', 'Karpet');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_layanan`
--

CREATE TABLE `tbl_layanan` (
  `id_layanan` varchar(5) NOT NULL,
  `nama_layanan` varchar(50) DEFAULT NULL,
  `id_barang` varchar(5) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_layanan`
--

INSERT INTO `tbl_layanan` (`id_layanan`, `nama_layanan`, `id_barang`, `harga`) VALUES
('LV001', 'Cuci (Kiloan)', 'B-001', 8000),
('LV002', 'Setrika (Kiloan)', 'B-001', 8000),
('LV003', 'Cuci + Setrika (Kiloan)', 'B-001', 15000),
('LV004', 'Laundry Satuan (Pakaian)', 'B-002', 5000),
('LV005', 'Laundry Satuan (Jaket)', 'B-003', 7000),
('LV006', 'Laundry Satuan (Bed Cover)', 'B-004', 10000),
('LV007', 'Laundry Satuan (Karpet)', 'B-005', 12500);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_paket`
--

CREATE TABLE `tbl_paket` (
  `id_paket` varchar(3) NOT NULL,
  `nama_paket` varchar(30) DEFAULT NULL,
  `multiplier` float DEFAULT NULL,
  `prioritas` int(11) DEFAULT NULL,
  `durasi` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_paket`
--

INSERT INTO `tbl_paket` (`id_paket`, `nama_paket`, `multiplier`, `prioritas`, `durasi`) VALUES
('P01', 'REGULER', 1, 4, '3 Hari'),
('P02', 'EXPRESS', 1.5, 3, '2 Hari'),
('P03', 'VIP', 2, 2, '1 Hari'),
('P04', 'SUPER VIP', 2.5, 1, '6 Jam');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pelanggan`
--

CREATE TABLE `tbl_pelanggan` (
  `id_pel` varchar(8) NOT NULL,
  `nama_pel` varchar(30) DEFAULT NULL,
  `alamat` varchar(50) DEFAULT NULL,
  `no_telp` varchar(13) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_pelanggan`
--

INSERT INTO `tbl_pelanggan` (`id_pel`, `nama_pel`, `alamat`, `no_telp`) VALUES
('u-000001', 'amsori', 'jl raya pemda', '089647217473'),
('u-000002', 'Muhammad Faiz', 'Jl Rambutan No 90, Bantar Kemang, Bogor Timur ', '089745789865'),
('u-000003', 'Ananda', 'Cilebut, Bogor', '087746578945'),
('u-000004', 'Ridho', 'Jln. Bintara 12 A, Bekasi Barat', '08881913126'),
('u-000005', 'Pian', 'Kemang, Bogor', '089694870338'),
('u-000006', 'Ahmad', 'Salabenda, Bogor', '08121818990'),
('u-000007', 'Alfian', 'Kemang, Bogor', '089605990544'),
('u-000008', 'Purba', 'Semplak, Bogor', '082198880933'),
('u-000009', 'Alfin', 'Parung, Bogor', '081243659986'),
('u-000010', 'yudi', 'parung,bogor', '087471274848'),
('u-000011', 'totom', 'cimanggu,bogor', '089732773388'),
('u-000012', 'Novi', 'Ciampea, Bogor', '088788556693');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_petugas`
--

CREATE TABLE `tbl_petugas` (
  `id_petugas` varchar(8) NOT NULL,
  `nama_petugas` varchar(30) DEFAULT NULL,
  `username` varchar(10) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_petugas`
--

INSERT INTO `tbl_petugas` (`id_petugas`, `nama_petugas`, `username`, `password`, `role`) VALUES
('crew-001', 'Administrator', 'Admin', '$2y$10$qzBpP2s6KKYVK82zxklETup6sZUfY6xQgTmdUeajYaQo8EMbfrQKC', 'admin'),
('crew-002', 'Karyawan', 'Kar', '$2y$10$oWe0Gnod2JZAd3sJSGVATe7OFVN8JaYyXHDcuXAxCzO87CSX/rLY2', 'karyawan');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_status`
--

CREATE TABLE `tbl_status` (
  `id_status` varchar(3) NOT NULL,
  `keterangan` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_status`
--

INSERT INTO `tbl_status` (`id_status`, `keterangan`) VALUES
('S01', 'Baru'),
('S02', 'Dalam Proses'),
('S03', 'Selesai'),
('S04', 'Diambil');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaksi`
--

CREATE TABLE `tbl_transaksi` (
  `id_transaksi` varchar(8) NOT NULL,
  `id_pel` varchar(8) DEFAULT NULL,
  `id_layanan` varchar(5) DEFAULT NULL,
  `id_paket` varchar(3) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `tgl_masuk` date DEFAULT NULL,
  `id_status` varchar(3) DEFAULT NULL,
  `id_petugas` varchar(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_transaksi`
--

INSERT INTO `tbl_transaksi` (`id_transaksi`, `id_pel`, `id_layanan`, `id_paket`, `qty`, `tgl_masuk`, `id_status`, `id_petugas`) VALUES
('tr000001', 'u-000001', 'LV001', 'P01', 5, '2022-06-11', 'S04', 'crew-002'),
('tr000002', 'u-000002', 'LV002', 'P02', 2, '2022-06-11', 'S04', 'crew-002'),
('tr000003', 'u-000003', 'LV003', 'P03', 3, '2022-06-11', 'S04', 'crew-002'),
('tr000004', 'u-000004', 'LV004', 'P04', 2, '2022-06-11', 'S04', 'crew-002'),
('tr000005', 'u-000005', 'LV005', 'P04', 2, '2022-06-11', 'S04', 'crew-002'),
('tr000006', 'u-000006', 'LV006', 'P03', 12, '2022-06-11', 'S04', 'crew-002'),
('tr000007', 'u-000007', 'LV007', 'P02', 3, '2022-06-11', 'S04', 'crew-002'),
('tr000008', 'u-000008', 'LV007', 'P01', 2, '2022-06-11', 'S04', 'crew-002'),
('tr000009', 'u-000009', 'LV006', 'P01', 15, '2022-06-11', 'S04', 'crew-002'),
('tr000010', 'u-000010', 'LV005', 'P01', 4, '2022-06-11', 'S04', 'crew-002'),
('tr000011', 'u-000011', 'LV004', 'P02', 5, '2022-06-11', 'S04', 'crew-002'),
('tr000012', 'u-000012', 'LV003', 'P03', 2, '2022-06-11', 'S04', 'crew-002'),
('tr000013', 'u-000001', 'LV007', 'P01', 2, '2022-06-12', 'S04', 'crew-002'),
('tr000014', 'u-000002', 'LV006', 'P02', 3, '2022-06-12', 'S04', 'crew-002'),
('tr000015', 'u-000003', 'LV005', 'P03', 3, '2022-06-12', 'S04', 'crew-002'),
('tr000016', 'u-000004', 'LV004', 'P04', 4, '2022-06-12', 'S04', 'crew-002'),
('tr000017', 'u-000005', 'LV003', 'P04', 1, '2022-06-12', 'S04', 'crew-002'),
('tr000018', 'u-000006', 'LV002', 'P03', 10, '2022-06-12', 'S04', 'crew-002'),
('tr000019', 'u-000007', 'LV001', 'P02', 3, '2022-06-12', 'S04', 'crew-002'),
('tr000020', 'u-000008', 'LV001', 'P01', 5, '2022-06-12', 'S04', 'crew-002'),
('tr000021', 'u-000009', 'LV002', 'P01', 15, '2022-06-12', 'S04', 'crew-002'),
('tr000022', 'u-000010', 'LV003', 'P01', 1, '2022-06-12', 'S04', 'crew-002'),
('tr000023', 'u-000011', 'LV004', 'P02', 5, '2022-06-12', 'S04', 'crew-002'),
('tr000024', 'u-000012', 'LV005', 'P03', 4, '2022-06-12', 'S04', 'crew-002'),
('tr000025', 'u-000001', 'LV001', 'P04', 13, '2022-06-13', 'S04', 'crew-002'),
('tr000026', 'u-000002', 'LV002', 'P03', 2, '2022-06-13', 'S04', 'crew-002'),
('tr000027', 'u-000003', 'LV003', 'P02', 3, '2022-06-13', 'S04', 'crew-002'),
('tr000028', 'u-000004', 'LV004', 'P01', 2, '2022-06-13', 'S04', 'crew-002'),
('tr000029', 'u-000005', 'LV005', 'P01', 2, '2022-06-13', 'S04', 'crew-002'),
('tr000030', 'u-000006', 'LV006', 'P02', 10, '2022-06-13', 'S04', 'crew-002'),
('tr000031', 'u-000007', 'LV007', 'P03', 3, '2022-06-13', 'S04', 'crew-002'),
('tr000032', 'u-000008', 'LV007', 'P04', 2, '2022-06-13', 'S04', 'crew-002'),
('tr000033', 'u-000009', 'LV006', 'P04', 11, '2022-06-13', 'S04', 'crew-002'),
('tr000034', 'u-000010', 'LV005', 'P03', 4, '2022-06-13', 'S04', 'crew-002'),
('tr000035', 'u-000011', 'LV004', 'P02', 5, '2022-06-13', 'S04', 'crew-002'),
('tr000036', 'u-000012', 'LV003', 'P01', 2, '2022-06-13', 'S04', 'crew-002'),
('tr000037', 'u-000001', 'LV007', 'P04', 9, '2022-06-14', 'S04', 'crew-002'),
('tr000038', 'u-000002', 'LV006', 'P03', 2, '2022-06-14', 'S04', 'crew-002'),
('tr000039', 'u-000003', 'LV005', 'P02', 3, '2022-06-14', 'S04', 'crew-002'),
('tr000040', 'u-000004', 'LV004', 'P01', 5, '2022-06-14', 'S04', 'crew-002'),
('tr000041', 'u-000005', 'LV003', 'P01', 2, '2022-06-14', 'S04', 'crew-002'),
('tr000042', 'u-000006', 'LV002', 'P02', 8, '2022-06-14', 'S04', 'crew-002'),
('tr000043', 'u-000007', 'LV001', 'P03', 3, '2022-06-14', 'S04', 'crew-002'),
('tr000044', 'u-000008', 'LV001', 'P04', 5, '2022-06-14', 'S04', 'crew-002'),
('tr000045', 'u-000009', 'LV002', 'P04', 11, '2022-06-14', 'S04', 'crew-002'),
('tr000046', 'u-000010', 'LV003', 'P03', 4, '2022-06-14', 'S04', 'crew-002'),
('tr000047', 'u-000011', 'LV004', 'P02', 1, '2022-06-14', 'S04', 'crew-002'),
('tr000048', 'u-000012', 'LV005', 'P01', 4, '2022-06-14', 'S04', 'crew-002'),
('tr000049', 'u-000001', 'LV001', 'P01', 5, '2022-06-15', 'S03', 'crew-002'),
('tr000050', 'u-000002', 'LV002', 'P02', 2, '2022-06-15', 'S04', 'crew-002'),
('tr000051', 'u-000003', 'LV003', 'P03', 3, '2022-06-15', 'S04', 'crew-002'),
('tr000052', 'u-000004', 'LV004', 'P04', 2, '2022-06-15', 'S04', 'crew-002'),
('tr000053', 'u-000005', 'LV005', 'P04', 2, '2022-06-15', 'S04', 'crew-002'),
('tr000054', 'u-000006', 'LV006', 'P03', 12, '2022-06-15', 'S04', 'crew-002'),
('tr000055', 'u-000007', 'LV007', 'P02', 3, '2022-06-15', 'S04', 'crew-002'),
('tr000056', 'u-000008', 'LV001', 'P01', 2, '2022-06-15', 'S03', 'crew-002'),
('tr000057', 'u-000009', 'LV002', 'P01', 15, '2022-06-15', 'S03', 'crew-002'),
('tr000058', 'u-000010', 'LV003', 'P01', 4, '2022-06-15', 'S03', 'crew-002'),
('tr000059', 'u-000011', 'LV004', 'P02', 5, '2022-06-15', 'S04', 'crew-002'),
('tr000060', 'u-000012', 'LV005', 'P03', 2, '2022-06-15', 'S04', 'crew-002'),
('tr000061', 'u-000001', 'LV007', 'P01', 2, '2022-06-16', 'S02', 'crew-002'),
('tr000062', 'u-000002', 'LV006', 'P02', 3, '2022-06-16', 'S03', 'crew-001'),
('tr000063', 'u-000003', 'LV005', 'P03', 3, '2022-06-16', 'S04', 'crew-002'),
('tr000064', 'u-000004', 'LV004', 'P04', 4, '2022-06-16', 'S04', 'crew-002'),
('tr000065', 'u-000005', 'LV003', 'P04', 1, '2022-06-16', 'S04', 'crew-002'),
('tr000066', 'u-000006', 'LV002', 'P03', 10, '2022-06-16', 'S04', 'crew-002'),
('tr000067', 'u-000007', 'LV001', 'P02', 3, '2022-06-16', 'S03', 'crew-001'),
('tr000068', 'u-000008', 'LV007', 'P01', 5, '2022-06-16', 'S02', 'crew-002'),
('tr000069', 'u-000009', 'LV006', 'P01', 15, '2022-06-16', 'S02', 'crew-002'),
('tr000070', 'u-000010', 'LV005', 'P01', 1, '2022-06-16', 'S02', 'crew-002'),
('tr000071', 'u-000011', 'LV004', 'P02', 5, '2022-06-16', 'S03', 'crew-001'),
('tr000072', 'u-000012', 'LV003', 'P03', 4, '2022-06-16', 'S04', 'crew-002'),
('tr000073', 'u-000001', 'LV007', 'P04', 9, '2022-06-17', 'S04', 'crew-002'),
('tr000074', 'u-000002', 'LV006', 'P03', 2, '2022-06-17', 'S04', 'crew-002'),
('tr000075', 'u-000003', 'LV005', 'P02', 3, '2022-06-17', 'S02', 'crew-001'),
('tr000076', 'u-000004', 'LV004', 'P01', 5, '2022-06-17', 'S02', 'crew-002'),
('tr000077', 'u-000005', 'LV003', 'P04', 2, '2022-06-17', 'S04', 'crew-002'),
('tr000078', 'u-000006', 'LV002', 'P03', 8, '2022-06-17', 'S04', 'crew-002'),
('tr000079', 'u-000007', 'LV001', 'P02', 3, '2022-06-17', 'S02', 'crew-001'),
('tr000080', 'u-000008', 'LV001', 'P01', 5, '2022-06-17', 'S02', 'crew-002'),
('tr000081', 'u-000009', 'LV002', 'P01', 11, '2022-06-17', 'S02', 'crew-002'),
('tr000082', 'u-000010', 'LV003', 'P02', 4, '2022-06-17', 'S02', 'crew-001'),
('tr000083', 'u-000011', 'LV004', 'P03', 1, '2022-06-17', 'S04', 'crew-002'),
('tr000084', 'u-000012', 'LV005', 'P04', 4, '2022-06-17', 'S04', 'crew-002'),
('tr000085', 'u-000003', 'LV003', 'P01', 3, '2022-06-18', 'S01', 'crew-002'),
('tr000086', 'u-000004', 'LV002', 'P02', 2, '2022-06-18', 'S01', 'crew-002'),
('tr000087', 'u-000006', 'LV005', 'P01', 5, '2022-06-18', 'S01', 'crew-002');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_id_toko`
--
ALTER TABLE `tbl_id_toko`
  ADD PRIMARY KEY (`nama_toko`);

--
-- Indexes for table `tbl_jenis_barang`
--
ALTER TABLE `tbl_jenis_barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `tbl_layanan`
--
ALTER TABLE `tbl_layanan`
  ADD PRIMARY KEY (`id_layanan`),
  ADD KEY `id_barang` (`id_barang`);

--
-- Indexes for table `tbl_paket`
--
ALTER TABLE `tbl_paket`
  ADD PRIMARY KEY (`id_paket`);

--
-- Indexes for table `tbl_pelanggan`
--
ALTER TABLE `tbl_pelanggan`
  ADD PRIMARY KEY (`id_pel`);

--
-- Indexes for table `tbl_petugas`
--
ALTER TABLE `tbl_petugas`
  ADD PRIMARY KEY (`id_petugas`);

--
-- Indexes for table `tbl_status`
--
ALTER TABLE `tbl_status`
  ADD PRIMARY KEY (`id_status`);

--
-- Indexes for table `tbl_transaksi`
--
ALTER TABLE `tbl_transaksi`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `id_pel` (`id_pel`),
  ADD KEY `id_layanan` (`id_layanan`),
  ADD KEY `id_paket` (`id_paket`),
  ADD KEY `id_status` (`id_status`),
  ADD KEY `id_petugas` (`id_petugas`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_layanan`
--
ALTER TABLE `tbl_layanan`
  ADD CONSTRAINT `tbl_layanan_ibfk_1` FOREIGN KEY (`id_barang`) REFERENCES `tbl_jenis_barang` (`id_barang`);

--
-- Constraints for table `tbl_transaksi`
--
ALTER TABLE `tbl_transaksi`
  ADD CONSTRAINT `tbl_transaksi_ibfk_1` FOREIGN KEY (`id_pel`) REFERENCES `tbl_pelanggan` (`id_pel`),
  ADD CONSTRAINT `tbl_transaksi_ibfk_2` FOREIGN KEY (`id_layanan`) REFERENCES `tbl_layanan` (`id_layanan`),
  ADD CONSTRAINT `tbl_transaksi_ibfk_3` FOREIGN KEY (`id_paket`) REFERENCES `tbl_paket` (`id_paket`),
  ADD CONSTRAINT `tbl_transaksi_ibfk_4` FOREIGN KEY (`id_status`) REFERENCES `tbl_status` (`id_status`),
  ADD CONSTRAINT `tbl_transaksi_ibfk_5` FOREIGN KEY (`id_petugas`) REFERENCES `tbl_petugas` (`id_petugas`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
