<div class="row">
  <div class="col" style="height: 50px">
    <!-- Heder -->
    <header id="header">
      <div class="container-fluid p-0 fixed-top d-flex flex-row justify-content-between bg-opacity-50" style="height: 50px;">
        <div id="kiri">
          <div class="dropdown d-inline-block d-md-none p-2">
            <i class="bi bi-list dropdown-toggle display-6" data-bs-toggle="dropdown"></i>
            <div class="dropdown-menu">
              <a href="../dashboard/" class="dropdown-item">Dashboard</a>
              <a href="../layanan/" class="dropdown-item">Layanan</a>
              <a href="../pelanggan/" class="dropdown-item">Pelanggan</a>
              <a href="../transaksi/" class="dropdown-item">Transaksi</a>

            </div>
          </div>
          <!-- <img src="../assets/img/LaViola_Circle.png" alt="" class="img-fluid h-100 d-none d-md-block px-4" /> -->
        </div>

        <div id="kanan">
          <?php
          $id_petugas_session = $_SESSION['login'];
          $namaPetugas = query("SELECT * FROM tbl_petugas WHERE id_petugas='$id_petugas_session'");
          ?>
          <h3 class="d-none d-md-inline p-2">Welcome, <?= $namaPetugas[0]['nama_petugas']; ?></h3>
          <div class="dropdown d-inline-block p-2 p-md-0">
            <i class="bi-person-circle dropdown-toggle display-6" data-bs-toggle="dropdown"></i>
            <div class="dropdown-menu">
              <a href="../toko/" class="dropdown-item">Detail Toko</a>
              <a href="../petugas/" class="dropdown-item">Petugas</a>
              <a href="../logout/" class="dropdown-item">Logout</a>
            </div>
          </div>
        </div>

      </div>
    </header>
  </div>
</div>