<div class="row">
  <div class="col-md-2 p-0 m-0 d-md-block d-none position-fixed" style="z-index: 9999;">
    <nav id="sidebar">
      <div class="container-fluid p-0">
        <img src="../assets/img/LaViola_transparent.png" alt="" class="img-fluid w-100 d-none d-md-block px-4 py-2" />
        <div class="navbar p-0">
          <div class="navbar-nav text-uppercase mx-auto w-100 py-2">
            <div class="nav-item">
              <a href="../dashboard/" class="nav-link text-start px-4 text-white <?php echo $dashboardNavStatus; ?>"><i class="bi bi-pie-chart-fill"></i> Dashboard</a>
            </div>
            <div class="nav-item">
              <a href="../layanan/" class="nav-link text-start px-4 text-white <?php echo $layananNavStatus; ?>"><i class="bi bi-patch-check-fill"></i> Layanan</a>
            </div>
            <div class="nav-item">
              <a href="../pelanggan/" class="nav-link text-start px-4 text-white <?php echo $pelangganNavStatus; ?>"><i class="bi bi-person-fill"></i> Pelanggan</a>
            </div>
            <div class="nav-item">
              <a href="../transaksi/" class="nav-link text-start px-4 text-white <?php echo $transaksiNavStatus; ?>"><i class="bi bi-cash-coin"></i> Transaksi</a>
            </div>
          </div>
        </div>
      </div>
    </nav>
  </div>
</div>