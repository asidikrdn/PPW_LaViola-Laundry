<?php
// Autentifikasi Session
session_start();
if (!isset($_SESSION['login'])) {
  header('Location: ../login');
  exit;
}

require '../functions.php';

if (isset($_POST['tambah_pelanggan'])) {
  if (tambah_pelanggan($_POST) > 0) {
    echo "
          <script>
              alert('Data pelanggan berhasil ditambahkan !');
              document.location.href = 'index.php';
          </script>
      ";
  } else {
    echo "
          <script>
              alert('Data pelanggan gagal ditambahkan !');
              document.location.href = 'index.php';
          </script>
      ";
  }
}
?>

<!DOCTYPE html>
<html lang="en">
<!-- Head -->
<?php include '../partial/head.php'; ?>

<body>
  <div class="container-fluid">
    <!-- Sidebar Web Version-->
    <?php include '../partial/sidebar.php'; ?>
    <!-- Header -->
    <?php include '../partial/header.php'; ?>
    <div class="row">
      <div class="col-md-10 offset-md-2 p-0">
        <div class="container-fluid p-0">
          <!-- Main Content -->
          <main id="kontenUtama">
            <div class="row g-0">
              <div class="col-12">
                <div class="px-2 d-flex flex-row justify-content-between bg-light">
                  <h4>Form Tambah Pelanggan</h4>
                  <!-- <h4>Tanggal : xx - xx - xxxx</h4> -->
                  <a href="index.php" class="btn btn-secondary my-1">Kembali</a>
                </div>
              </div>
            </div>

            <!-- Form -->
            <div class="row g-0">
              <div class="col-12 p-3">
                <div class="container-fluid">
                  <div class="d-flex flex-row justify-content-center align-items-center" style="height: 100vh;">
                    <div class="div">
                      <form action="" method="post">
                        <label for="nama_pel">Nama Pelanggan :</label>
                        <input type="text" name="nama_pel" id="nama_pel" maxlength="30" required class="form-control">
                        <br>
                        <label for="alamat">Alamat :</label>
                        <textarea name="alamat" id="alamat" cols="30" rows="10" maxlength="50" required class="form-control"></textarea>
                        <br>
                        <label for="no_telp">No Telepon :</label>
                        <input type="tel" name="no_telp" id="no_telp" maxlength="12" required class="form-control">
                        <br>
                        <button type="submit" name="tambah_pelanggan" id="tambah_pelanggan" class="btn btn-primary">Tambah Pelanggan</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </main>
        </div>
      </div>
    </div>
  </div>

</body>
<script src="../assets/js/bootstrap.bundle.min.js"></script>

</html>