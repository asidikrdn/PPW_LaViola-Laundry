<?php
// Autentifikasi Session
session_start();
if (!isset($_SESSION['login'])) {
  header('Location: ../login');
  exit;
}

require '../functions.php';

$pelanggan = query("SELECT * FROM tbl_pelanggan");


// Mengaktifkan class actiive pada sidebar
$pelangganNavStatus = "active";
?>
<!DOCTYPE html>
<html lang="en">
<!-- Head -->
<?php include '../partial/head.php'; ?>

<body>
  <div class="container-fluid">
    <!-- Sidebar Web Version-->
    <?php include '../partial/sidebar.php'; ?>
    <!-- Header -->
    <?php include '../partial/header.php'; ?>
    <div class="row">
      <div class="col-md-10 offset-md-2 p-0">
        <div class="container-fluid p-0">
          <!-- Main Content -->
          <main id="kontenUtama">
            <div class="row g-0">
              <div class="col-12">
                <div class="px-2 d-flex flex-row justify-content-between bg-light">
                  <h4>Pelanggan</h4>
                  <!-- <h4>Tanggal : xx - xx - xxxx</h4> -->
                </div>
              </div>
            </div>

            <!-- Tabel Transaksi -->
            <div class="row g-0">
              <div class="col-12 p-3">
                <a href="tambah_pelanggan.php" class="btn btn-primary ms-0 my-2"><i class="bi bi-plus-circle"></i> Pelanggan</a>
                <table class="mx-auto table bg-white">
                  <tr class="table-danger text-center">
                    <th>No</th>
                    <th>Nama Pelanggan</th>
                    <th>Alamat</th>
                    <th>No. HP</th>
                    <th></th>
                  </tr>
                  <?php $i = 1; ?>
                  <?php foreach ($pelanggan as $customer) : ?>
                    <tr class="text-center">
                      <td><?= $i; ?></td>
                      <td><?= $customer['nama_pel']; ?></td>
                      <td><?= $customer['alamat']; ?></td>
                      <td><?= $customer['no_telp']; ?></td>
                      <td>
                        <a href="edit_pelanggan.php?id_pel=<?php echo $customer['id_pel']; ?>" class="btn btn-warning"><i class="bi bi-pencil"></i></a>
                      </td>
                    </tr>
                  <?php $i++;
                  endforeach; ?>
                </table>
              </div>
            </div>

          </main>
        </div>
      </div>
    </div>
  </div>
</body>
<script src="../assets/js/bootstrap.bundle.min.js"></script>

</html>