<?php
// Koneksi ke Database
$conn = mysqli_connect("localhost", "root", "", "db_laviola");

// Mengambil tanggal dari sistem
date_default_timezone_set("Asia/Jakarta");
$date = date('d');
$moon = date('m');
$year = date('Y');

// Membuat fungsi konversi bulan ke nama bulan
if ($moon == 1) {
  $namabulan = 'Januari';
} else if ($moon == 2) {
  $namabulan = 'Februari';
} else if ($moon == 3) {
  $namabulan = 'Maret';
} else if ($moon == 4) {
  $namabulan = 'April';
} else if ($moon == 5) {
  $namabulan = 'Mei';
} else if ($moon == 6) {
  $namabulan = 'Juni';
} else if ($moon == 7) {
  $namabulan = 'Juli';
} else if ($moon == 8) {
  $namabulan = 'Agustus';
} else if ($moon == 9) {
  $namabulan = 'September';
} else if ($moon == 10) {
  $namabulan = 'Oktober';
} else if ($moon == 11) {
  $namabulan = 'November';
} else if ($moon == 12) {
  $namabulan = 'Desember';
}


// Membuat functions query untuk mengambil data dari database
function query($query)
{
  global $conn;
  $result = mysqli_query($conn, $query);
  $rows = [];
  while ($row = mysqli_fetch_assoc($result)) {
    $rows[] = $row;
  }
  return $rows;
}



// Membuat functions auto increment untuk id
function id_auto_increment($id_terakhir, $panjang_kode, $panjang_angka)
{
  // Mengambil nilai kode
  $kode = substr($id_terakhir, 0, $panjang_kode);

  // Menagmbil nilai angka
  $angka = substr($id_terakhir, $panjang_kode, $panjang_angka);

  // Menambah nilai angka dengan 1
  $angka_baru = str_repeat("0", $panjang_angka - strlen($angka + 1)) . ($angka + 1);

  $id_baru = $kode . $angka_baru;

  return $id_baru;
}


// Membuat function untuk konversi angka biasa dengan format rupiah
function rupiah($angka){
	
	$hasil_rupiah = "Rp " . number_format($angka,2,',','.');
	return $hasil_rupiah;
 
}





// =================================================================
// PETUGAS
// =================================================================
// Membuat functions register karyawan
function register($data)
{
  global $conn;

  // $id_petugas = htmlspecialchars($data['id_petugas']);
  $nama_petugas = htmlspecialchars($data['nama_petugas']);
  $username = htmlspecialchars($data['username']);
  $password = mysqli_real_escape_string($conn, $data['password']);
  $password2 = mysqli_real_escape_string($conn, $data['password2']);
  $role = htmlspecialchars($data['role']);

  // Cek Konfirmasi Password
  if ($password !== $password2) {
    echo "<script>
          alert('Konfirmasi Password tidak sesuai');
      </script>";
    return false;
  }

  // Melihat id_petugas terakhir
  $cek_id = query("SELECT MAX(id_petugas) as id_petugas FROM tbl_petugas");
  $id_terakhir = $cek_id[0]['id_petugas'];
  // var_dump($cek_id);
  // var_dump($id_terakhir);

  if ($id_terakhir == null) {
    $id_petugas = "crew-001";
  } else {
    $id_petugas = id_auto_increment($id_terakhir, 5, 3);
  }

  // cek username sudah ada atau belum, jika sudah ada maka tidak boleh dimasukkan
  $result = mysqli_query($conn, "SELECT username FROM tbl_petugas WHERE username = '$username'");
  // var_dump($result);

  if (mysqli_fetch_assoc($result)) {
    echo "<script>
            alert('Username sudah terdaftar, Gunakan username lainnya !');
        </script>";
    return false;
  }

  // enkripsi password
  $password = password_hash($password, PASSWORD_DEFAULT);
  // var_dump($password);

  // membuat query insert database
  $query = "INSERT INTO tbl_petugas VALUES ('$id_petugas','$nama_petugas','$username','$password','$role')";

  // tambahkan user baru ke database
  mysqli_query($conn, $query);
  return mysqli_affected_rows($conn);
}

// Membuat function update pelanggan 
function update_petugas($data)
{
  global $conn;
  
  $id_petugas = $_GET["id_petugas"];

  $nama_petugas = htmlspecialchars($data['nama_petugas']);
  $username = htmlspecialchars($data['username']);
  // $password = mysqli_real_escape_string($conn, $data['password']);
  // $password2 = mysqli_real_escape_string($conn, $data['password2']);
  $role = htmlspecialchars($data['role']);

  // enkripsi password
  // $password = password_hash($password, PASSWORD_DEFAULT);
  // var_dump($password);

  // membuat query insert database
  $query = "UPDATE tbl_petugas 
            SET 
            id_petugas = '$id_petugas',
            nama_petugas = '$nama_petugas',
            username = '$username',
            role = '$role')";

  // tambahkan user baru ke database
  mysqli_query($conn, $query);
  return mysqli_affected_rows($conn);
}
// =================================================================
// AKHIR PETUGAS
// =================================================================





// =================================================================
// PELANGGAN
// =================================================================
// Membuat functions tambah pelanggan
function tambah_pelanggan($data)
{
  global $conn;

  $nama_pel = htmlspecialchars($data['nama_pel']);
  $alamat = htmlspecialchars($data['alamat']);
  $no_telp = htmlspecialchars($data['no_telp']);

  // Melihat id_pel terakhir
  $cek_id = query("SELECT MAX(id_pel) as id_pel FROM tbl_pelanggan");
  $id_terakhir = $cek_id[0]['id_pel'];
  // var_dump($cek_id);
  // var_dump($id_terakhir);

  if ($id_terakhir == null) {
    $id_pel = "u-000001";
  } else {
    $id_pel = id_auto_increment($id_terakhir, 2, 6);
  }

  // membuat query insert database
  $query = "INSERT INTO tbl_pelanggan VALUES ('$id_pel','$nama_pel','$alamat','$no_telp')";

  // tambahkan user baru ke database
  mysqli_query($conn, $query);
  return mysqli_affected_rows($conn);
}

// Membuat function update pelanggan 
function update_pelanggan($data)
{
  global $conn;
  
  $id_pel = $_GET["id_pel"];

  $nama_pel = htmlspecialchars($data['nama_pel']);
  $alamat = htmlspecialchars($data['alamat']);
  $no_telp = htmlspecialchars($data['no_telp']);

  // membuat query insert database
  $query = "UPDATE tbl_pelanggan 
            SET 
            id_pel = '$id_pel',
            nama_pel = '$nama_pel',
            alamat = '$alamat',
            no_telp = '$no_telp'
            WHERE id_pel = '$id_pel'";

  // tambahkan user baru ke database
  mysqli_query($conn, $query);
  return mysqli_affected_rows($conn);
}
// =================================================================
// AKHIR PELANGGAN
// =================================================================





// =================================================================
// LAYANAN
// =================================================================
// Membuat functions tambah layanan
function tambah_layanan($data)
{
  global $conn;

  $nama_layanan = htmlspecialchars($data['nama_layanan']);
  $id_barang = htmlspecialchars($data['id_barang']);
  $harga = htmlspecialchars($data['harga']);

  // Melihat nama_barang
  $cek_nama_barang = query("SELECT * FROM tbl_jenis_barang WHERE id_barang = '$id_barang'");
  $nama_layanan = htmlspecialchars($data['nama_layanan'] . ' (' . $cek_nama_barang[0]['nama_barang'] . ')');

  // cek Layanan sudah ada atau belum, jika sudah ada maka tidak boleh dimasukkan
  $result = mysqli_query($conn, "SELECT nama_layanan FROM tbl_layanan WHERE nama_layanan = '$nama_layanan'");
  // var_dump($result);

  if (mysqli_fetch_assoc($result)) {
    echo "<script>
            alert('Layanan sudah terdaftar, tambahkan layanan lainnya !');
        </script>";
    return false;
  }

  // Melihat id_layanan terakhir
  $cek_id = query("SELECT MAX(id_layanan) as id_layanan FROM tbl_layanan");
  $id_terakhir = $cek_id[0]['id_layanan'];
  // var_dump($cek_id);
  // var_dump($hasil);
  // var_dump($id_terakhir);

  if ($id_terakhir == null) {
    $id_layanan = "LV001";
  } else {
    $id_layanan = id_auto_increment($id_terakhir, 2, 3);
  }

  // membuat query
  $query = "INSERT INTO tbl_layanan VALUES ('$id_layanan', '$nama_layanan', '$id_barang', '$harga')";

  // tambahkan layanan baru ke database
  mysqli_query($conn, $query);
  return mysqli_affected_rows($conn);
}

// Membuat function update layanan 
function update_layanan($data)
{
  global $conn;

  $id_layanan = $_GET["id_lynn"];
  $nama_layanan = htmlspecialchars($data['nama_layanan']);
  $id_barang = htmlspecialchars($data['id_barang']);
  $harga = htmlspecialchars($data['harga']);

  // Melihat nama_barang
  $cek_nama_barang = query("SELECT * FROM tbl_jenis_barang WHERE id_barang = '$id_barang'");
  $nama_layanan = htmlspecialchars($data['nama_layanan'] . ' (' . $cek_nama_barang[0]['nama_barang'] . ')');
  
  // membuat query
  $query = "UPDATE tbl_layanan 
        SET 
          id_layanan ='$id_layanan', 
          nama_layanan = '$nama_layanan', 
          id_barang = '$id_barang', 
          harga = '$harga' 
          WHERE 
            id_layanan = '$id_layanan'";

  // tambahkan layanan baru ke database
  mysqli_query($conn, $query);
  return mysqli_affected_rows($conn);
}
// =================================================================
// AKHIR LAYANAN
// =================================================================




// =================================================================
// TRANSAKSI
// =================================================================
// Membuat function tambah transaksi
function tambah_transaksi($data)
{
  global $conn;

  $id_pel = htmlspecialchars($data['id_pel']);
  $id_layanan = htmlspecialchars($data['id_layanan']);
  $id_paket = htmlspecialchars($data['id_paket']);
  $qty = htmlspecialchars($data['qty']);
  $id_petugas = htmlspecialchars($data['id_petugas']);

  // Melihat id_transaksi terakhir
  $cek_id = query("SELECT MAX(id_transaksi) as id_transaksi FROM tbl_transaksi");
  $id_terakhir = $cek_id[0]['id_transaksi'];
  // var_dump($cek_id);
  // var_dump($id_terakhir);

  if ($id_terakhir == null) {
    $id_transaksi = "tr000001";
  } else {
    $id_transaksi = id_auto_increment($id_terakhir, 2, 6);
  }

  // Status, karena menambahkan transaksi baru, maka status transaksinya diisi dengan Baru
  $id_status = "S01";

  // Mengambil tanggal dari sistem
  global $date;
  global $moon;
  global $year;

  // Menentukan tanggal
  $tgl_masuk = "$year-$moon-$date";
  // if ($id_paket == 'P01') {
  //   $date+=3;
  //   $tgl_selesai =  "$year-$moon-$date";
  // }
  // else if ($id_paket == 'P02') {
  //   $date+=2;
  //   $tgl_selesai =  "$year-$moon-$date";
  // }
  // else if ($id_paket == 'P03') {
  //   $date+=1;
  //   $tgl_selesai =  "$year-$moon-$date";
  // }
  // else if ($id_paket == 'P04') {
  //   $date+=0;
  //   $tgl_selesai =  "$year-$moon-$date";
  // }


  // membuat query
  $query = "INSERT INTO tbl_transaksi VALUES ('$id_transaksi', '$id_pel', '$id_layanan', '$id_paket', '$qty', '$tgl_masuk', '$id_status', '$id_petugas')";

  // tambahkan transaksi baru ke database
  mysqli_query($conn, $query);
  return mysqli_affected_rows($conn);
}


// Membuat function update status 
function update_transaksi($data)
{
  global $conn;
  
  $cek_data = query("SELECT * FROM tbl_transaksi WHERE id_transaksi='$data'");

  $id_transaksi = $cek_data[0]['id_transaksi'];
  $id_pel = $cek_data[0]['id_pel'];
  $id_layanan = $cek_data[0]['id_layanan'];
  $id_paket = $cek_data[0]['id_paket'];
  $qty = $cek_data[0]['qty'];
  $tgl_masuk = $cek_data[0]['tgl_masuk'];
  // $id_petugas = $cek_data[0]['id_petugas'];
  $id_petugas = $_SESSION['login']; // menangkap id petugas berdasarkan session



  
  if ($cek_data[0]['id_status'] == 'S01'){
    $new_status = 'S02';
  }
  else if ($cek_data[0]['id_status'] == 'S02'){
    $new_status = 'S03';
  }
  else if ($cek_data[0]['id_status'] == 'S03'){
    $new_status = 'S04';
  };
  
  $query = "UPDATE tbl_transaksi 
          SET
            id_transaksi = '$id_transaksi',
            id_pel = '$id_pel',
            id_layanan = '$id_layanan',
            id_paket = '$id_paket',
            qty = '$qty',
            tgl_masuk = '$tgl_masuk',
            id_status = '$new_status',
            id_petugas = '$id_petugas'
          WHERE id_transaksi = '$data'";
  mysqli_query($conn, $query);
  return mysqli_affected_rows($conn);
}


// Membuat function hapus_transaksi 
function hapus_transaksi($data)
{
  global $conn;
  
  $query = "DELETE FROM tbl_transaksi 
          WHERE id_transaksi = '$data'";
  mysqli_query($conn, $query);
  return mysqli_affected_rows($conn);
}
// =================================================================
// AKHIR TRANSAKSI
// =================================================================


// =================================================================
// TOKO
// =================================================================
// Membuat function update data toko 
function update_toko($data)
{
  global $conn;

  $nama_toko_awal = $_GET["nama_toko"];
  $nama_toko = $data['nama_toko'];
  $telp_toko = $data['telp_toko'];
  $alamat_toko = $data['alamat_toko'];


  // membuat query
  $query = "UPDATE tbl_id_toko 
        SET 
          nama_toko ='$nama_toko', 
          telp_toko = '$telp_toko', 
          alamat_toko = '$alamat_toko'
          WHERE 
            nama_toko = '$nama_toko_awal'";

  // tambahkan layanan baru ke database
  mysqli_query($conn, $query);
  return mysqli_affected_rows($conn);
}

// =================================================================
// AKHIR TOKO
// =================================================================
?>