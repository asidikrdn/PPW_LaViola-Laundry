<?php
// Autentifikasi Session
session_start();
if (!isset($_SESSION['login'])) {
  header('Location: ../login');
  exit;
}

require '../functions.php';

// Menghitung jumlah pelanggan
$jumlahPelanggan = query('SELECT COUNT(tbl_pelanggan.id_pel) AS pelanggan
FROM tbl_pelanggan');
// Menghitung jumlah petugas
$jumlahPetugas = query('SELECT COUNT(tbl_petugas.id_petugas) AS petugas
FROM tbl_petugas');
// Menghitung jumlah order
$jumlahOrder = query('SELECT COUNT(tbl_transaksi.id_transaksi) as totalorder
FROM tbl_transaksi');

// Menghitung jumlah order baru
$jumlahOrderBaru = query("SELECT COUNT(tbl_transaksi.id_transaksi) as jumlahOrder
FROM tbl_transaksi, tbl_status
WHERE tbl_transaksi.id_status = tbl_status.id_status AND tbl_status.keterangan = 'Baru'");
// Menghitung jumlah order sedang diproses
$jumlahOrderProses = query("SELECT COUNT(tbl_transaksi.id_transaksi) as jumlahOrder
FROM tbl_transaksi, tbl_status
WHERE tbl_transaksi.id_status = tbl_status.id_status AND tbl_status.keterangan = 'Dalam Proses'");
// Menghitung jumlah order selesai
$jumlahOrderSelesai = query("SELECT COUNT(tbl_transaksi.id_transaksi) as jumlahOrder
FROM tbl_transaksi, tbl_status
WHERE tbl_transaksi.id_status = tbl_status.id_status AND tbl_status.keterangan = 'Selesai'");
// Menghitung jumlah order sudah diambil
$jumlahOrderDiambil = query("SELECT COUNT(tbl_transaksi.id_transaksi) as jumlahOrder
FROM tbl_transaksi, tbl_status
WHERE tbl_transaksi.id_status = tbl_status.id_status AND tbl_status.keterangan = 'Diambil'");

$judulChart = "Rekap Order Bulan " . $namabulan;
$dataPoints = array(
  array("y" => $jumlahOrderBaru[0]['jumlahOrder'], "indexLabel" => "Baru"),
  array("y" => $jumlahOrderProses[0]['jumlahOrder'], "indexLabel" => "Diproses"),
  array("y" => $jumlahOrderSelesai[0]['jumlahOrder'], "indexLabel" => "Selesai"),
  array("y" => $jumlahOrderDiambil[0]['jumlahOrder'], "indexLabel" => "Diambil"),
);



// Mengaktifkan class actiive pada sidebar
$dashboardNavStatus = "active";
?>
<!DOCTYPE html>
<html lang="en">
<!-- Head -->
<?php include '../partial/head.php'; ?>

<body>
  <div class="container-fluid">
    <!-- Sidebar Web Version-->
    <?php include '../partial/sidebar.php'; ?>
    <!-- Header -->
    <?php include '../partial/header.php'; ?>
    <div class="row">
      <div class="col-md-10 offset-md-2 p-0">
        <div class="container-fluid p-0">
          <!-- Main Content -->
          <main id="kontenUtama">
            <div class="row g-0">
              <div class="col-12">
                <div class="px-2 d-flex flex-row justify-content-between bg-light">
                  <h4>Dashboard</h4>
                  <h4>Tanggal : <?= $date ?> - <?= $moon ?> - <?= $year ?></h4>
                </div>
              </div>
            </div>

            <!-- Counter -->
            <div class="row g-0">
              <div class="col-lg-4 col-12 p-2">
                <div class="card bg-success">
                  <div class="card-body">
                    <h5 class="card-title text-black">Total Pelanggan</h5>
                    <div class="card-subtitle text-muted text-end"></div>
                    <div class="card-text mt-2 text-end">
                      <h1 class="display-3"><?= $jumlahPelanggan[0]['pelanggan']; ?></h1>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-4 col-12 p-2">
                <div class="card bg-primary">
                  <div class="card-body">
                    <h5 class="card-title text-black">Total Pegawai</h5>
                    <div class="card-subtitle text-muted text-end"></div>
                    <div class="card-text mt-2 text-end">
                      <h1 class="display-3"><?= $jumlahPetugas[0]['petugas']; ?></h1>
                    </div>
                  </div>
                </div>
              </div>
              <!-- <div class="col-lg-4 col-12 p-2">
                <div class="card bg-warning">
                  <div class="card-body">
                    <h5 class="card-title text-black">Order Baru</h5>
                    <div class="card-subtitle text-muted text-end"></div>
                    <div class="card-text mt-2 text-end">
                      <h1 class="display-3"><?= $jumlahOrderBaru[0]['jumlahOrder']; ?></h1>
                    </div>
                  </div>
                </div>
              </div> -->
              <div class="col-lg-4 col-12 p-2">
                <div class="card bg-info">
                  <div class="card-body">
                    <h5 class="card-title text-black">Total Order</h5>
                    <div class="card-subtitle text-muted text-end"></div>
                    <div class="card-text mt-2 text-end">
                      <h1 class="display-3"><?= $jumlahOrder[0]['totalorder']; ?></h1>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Rekap Order -->
            <div class="row g-0">
              <div class="col-12 px-2">
                <div class="card my-2">
                  <div class="card-body">
                    <div id="chartContainer" style="height: 370px; width: 100%"></div>
                  </div>
                </div>
              </div>
            </div>
          </main>
        </div>
      </div>
    </div>
  </div>
  <script src="../assets/js/bootstrap.bundle.min.js"></script>
  <script>
    window.onload = function() {

      var chart = new CanvasJS.Chart("chartContainer", {
        animationEnabled: true,
        exportEnabled: true,
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        title: {
          // text: "Rekap Order Bulanan"
          text: <?php echo json_encode($judulChart); ?>
        },
        axisY: {
          includeZero: true
        },
        data: [{
          type: "pie", //change type to column, bar, line, area, pie, etc
          //indexLabel: "{y}", //Shows y value on all Data Points
          indexLabelFontColor: "#5A5757",
          indexLabelPlacement: "outside",
          dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
        }]
      });
      chart.render();

    }
  </script>
  <script src="../assets/js/canvasjs.min.js"></script>
</body>

</html>