<?php
// Autentifikasi Session
session_start();
if (!isset($_SESSION['login'])) {
  header('Location: ../login');
  exit;
}

require '../functions.php';

if (isset($_POST['tambah_transaksi'])) {
  if (tambah_transaksi($_POST) > 0) {
    echo "
          <script>
              alert('Transaksi berhasil ditambahkan !');
              document.location.href = 'index.php';
          </script>
      ";
  } else {
    echo "
          <script>
              alert('Transaksi gagal ditambahkan !');
              document.location.href = 'index.php';
          </script>
      ";
  }
}

// Melihat id_paket
$cek_id_paket = query("SELECT * FROM tbl_paket");
// var_dump($cek_id_paket);

// Melihat id_layanan
$cek_id_layanan = query("SELECT * FROM tbl_layanan");
// var_dump($cek_id_layanan);

// Melihat id_pelanggan
$cek_id_pel = query("SELECT * FROM tbl_pelanggan");
// var_dump($cek_id_pel);

// Melihat id_petugas yang sedang aktif sessionnya
$petugas = $_SESSION['login']; // menangkap id petugas berdasarkan session
// $cek_nama_petugas = query("SELECT nama_petugas FROM tbl_petugas WHERE id_petugas = '$petugas'");  // mengambil detail nama petugas dari database berdasarkan id pada session
// $nama_petugas = $cek_nama_petugas[0]['nama_petugas']; // menyimpan nama petugas
// var_dump($petugas);
// var_dump($cek_nama_petugas);
// var_dump($nama_petugas);
?>
<!DOCTYPE html>
<html lang="en">
<!-- Head -->
<?php include '../partial/head.php'; ?>

<body>
  <div class="container-fluid">
    <!-- Sidebar Web Version-->
    <?php include '../partial/sidebar.php'; ?>
    <!-- Header -->
    <?php include '../partial/header.php'; ?>
    <div class="row">
      <div class="col-md-10 offset-md-2 p-0">
        <div class="container-fluid p-0">
          <!-- Main Content -->
          <main id="kontenUtama">
            <div class="row g-0">
              <div class="col-12">
                <div class="px-2 d-flex flex-row justify-content-between bg-light">
                  <h4>Form Tambah Transaksi</h4>
                  <!-- <h4>Tanggal : xx - xx - xxxx</h4> -->
                  <a href="index.php" class="btn btn-secondary my-1">Kembali</a>
                </div>
              </div>
            </div>

            <!-- Form -->
            <div class="row g-0">
              <div class="col-12 p-3">
                <div class="container-fluid">
                  <div class="d-flex flex-row justify-content-center align-items-center" style="height: 100vh;">
                    <div class="div">
                      <form action="" method="post">
                        <!-- Pelanggan -->
                        <label for="id_pel" class="form-label">Pelanggan :</label>
                        <select name="id_pel" id="id_pel" required class="form-control">
                          <option>-- Pilih Pelanggan --</option>
                          <?php foreach ($cek_id_pel as $pelanggan) : ?>
                            <option value="<?= $pelanggan['id_pel']; ?>">
                              <?= $pelanggan['nama_pel']; ?>
                            </option>
                          <?php endforeach; ?>
                        </select>
                        <a href="../pelanggan/tambah_pelanggan.php">Tambah Pelanggan</a>
                        <br>

                        <!-- Layanan -->
                        <label for="id_layanan" class="form-label">Pilih Layanan :</label>
                        <select name="id_layanan" id="id_layanan" required class="form-control">
                          <option>-- Pilih Layanan --</option>
                          <?php foreach ($cek_id_layanan as $layanan) : ?>
                            <option value="<?= $layanan['id_layanan']; ?>">
                              <?= $layanan['nama_layanan']; ?>
                            </option>
                          <?php endforeach; ?>
                        </select>
                        <br>

                        <!-- Paket -->
                        <label for="id_paket" class="form-label">Pilih Paket :</label>
                        <select name="id_paket" id="id_paket" required class="form-control">
                          <option>-- Pilih Paket --</option>
                          <?php foreach ($cek_id_paket as $paket) : ?>
                            <option value="<?= $paket['id_paket']; ?>">
                              <?= $paket['nama_paket']; ?>
                            </option>
                          <?php endforeach; ?>
                        </select>
                        <br>

                        <!-- Qty -->
                        <label for="qty" class="form-label">Qty (Kg / Pcs) :</label>
                        <input type="text" name="qty" id="qty" required class="form-control">
                        <br>

                        <!-- Tgl Masuk -->
                        <!-- Tgl Selesai -->
                        <!-- Total Harga -->

                        <!-- Petugas -->
                        <!-- <label for="id_petugas">Petugas :</label> -->
                        <input type="hidden" name="id_petugas" id="id_petugas" value="<?= $petugas; ?>">
                        <!-- <input type="text" value="<?= $nama_petugas; ?>" readonly> -->
                        <br>

                        <button type="submit" name="tambah_transaksi" id="tambah_transaksi" class="btn btn-primary">Tambah Transaksi</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </main>
        </div>
      </div>
    </div>
  </div>
</body>
<script src="../assets/js/bootstrap.bundle.min.js"></script>

</html>