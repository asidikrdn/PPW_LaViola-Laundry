<?php
// Autentifikasi Session
session_start();
if (!isset($_SESSION['login'])) {
  header('Location: ../login');
  exit;
}

require '../functions.php';

// Menangkap id_transaksi 
$id_transaksi = $_GET['id_trx'];

$toko = query('SELECT * FROM tbl_id_toko');

$transaksi = query("SELECT  tbl_pelanggan.nama_pel,
                            tbl_pelanggan.no_telp,
                            tbl_pelanggan.alamat,

                            tbl_transaksi.id_transaksi, 
                            tbl_transaksi.tgl_masuk, 
                             
                            tbl_paket.nama_paket, 
                            tbl_paket.multiplier, 
                            
                            tbl_layanan.nama_layanan, 
                            tbl_layanan.harga, 
                            
                            tbl_transaksi.qty
                    FROM 
                            tbl_pelanggan,
                            tbl_transaksi,  
                            tbl_paket, 
                            tbl_layanan
                    WHERE 
                            tbl_pelanggan.id_pel = tbl_transaksi.id_pel 
                        AND tbl_layanan.id_layanan = tbl_transaksi.id_layanan 
                        AND tbl_paket.id_paket = tbl_transaksi.id_paket
                        AND tbl_transaksi.id_transaksi = '$id_transaksi'
                    ");

?>

<!DOCTYPE html>
<html lang="en">
<!-- Head -->
<?php include '../partial/head.php'; ?>

<body>
  <div class="container-fluid d-flex flex-row justify-content-center w-100 bg-secondary p-2" style="height: 100vh;">
    <div class="container-fluid bg-white px-3" style="width: 900px">
      <div class="row">
        <div class="col-3">
          <div id="logo">
            <img src="../assets/img/LaViola_transparent.png" alt="" style="width: 200px; padding-right: 10px;" />
          </div>
        </div>
        <div class="col-5">
          <div id="id_toko">
            <h1><?=$toko[0]['nama_toko'];?></h1>
            <h5><?=$toko[0]['telp_toko'];?></h5>
            <h5><?=$toko[0]['alamat_toko'];?></h5>
          </div>
        </div>
        <div class="col-4 text-end">
          <h1 class="py-4">INVOICE</h1>
        </div>
      </div>
      <div class="row">
        <div class="col-12 mb-2">
          <hr>
        </div>
      </div>
      <div class="row">
        <div class="col-2">
          <h5><b>Customer</b></h5>
        </div>
        <div class="col-3 text-start">
          <p><?=$transaksi[0]['nama_pel'];?></p>
          <p><?=$transaksi[0]['no_telp'];?></p>
          <p><?=$transaksi[0]['alamat'];?></p>
        </div>
        <div class="col-2 offset-3">
          <p><b>No Transaksi</b></p>
          <p><b>Tanggal Order</b></p>
        </div>
        <div class="col-2 text-uppercase">
          <p><?=$transaksi[0]['id_transaksi'];?></p>
          <p><?=$transaksi[0]['tgl_masuk'];?></p>
        </div>
      </div>
      <div class="row">
        <div class="col-12 my-5 py-5">
          <table class="table mx-auto text-center">
            <tr class="table-secondary">
              <th>No</th>
              <th>Paket</th>
              <th>Layanan</th>
              <th>Qty</th>
              <th>Harga Satuan</th>
              <th>Total Harga</th>
            </tr>
            <tr class="table-light">
              <td>1</td>
              <td><?=$transaksi[0]['nama_paket'];?></td>
              <td><?=$transaksi[0]['nama_layanan'];?></td>
              <td><?=$transaksi[0]['qty'];?></td>
              <?php $hargaSatuan = $transaksi[0]['harga']*$transaksi[0]['multiplier']; $hargaTotal = $transaksi[0]['harga']*$transaksi[0]['multiplier']*$transaksi[0]['qty']; ?>
              <td><?=rupiah($hargaSatuan);?></td>
              <td><?=rupiah($hargaTotal);?></td>
            </tr>
          </table>
        </div>
      </div>
      <div class="row">
        <div class="col-12 mt-5 ">
          <h5>Keterangan :</h5>
          <ul>
            <li>Pengambilan cucian harus membawa nota</li>
            <li>Cucian luntur diluar tanggung jawab kami</li>
            <li>Hitung dan periksa sebelum meninggalkan store</li>
            <li>Klaim kehilangan/kerusakan setelah meninggalkan store tidak akan dilayani</li>
            <li>Cucian yang mengkerut karena sifat kain tidak kami ganti</li>
            <li>Cucian yang tidak diambil lebih dari 1 bulan bukan tanggung jawab kami</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</body>
<script src="../assets/js/bootstrap.bundle.min.js"></script>
<script>
  window.print();
</script>
</html>