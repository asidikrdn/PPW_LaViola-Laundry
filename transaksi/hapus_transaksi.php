<?php
// Autentifikasi Session
session_start();
if (!isset($_SESSION['login'])) {
  header('Location: ../login');
  exit;
}

require '../functions.php';

// Menangkap id_transaksi 
$id_transaksi = $_GET['id_trx'];

// Menjalankan fungsi hapus
if (hapus_transaksi($id_transaksi) > 0){
  echo "
      <script>
          alert('Transaksi berhasil dihapus !');
          document.location.href = 'index.php';
      </script>
  ";
} else {
  echo "
      <script>
          alert('Transaksi gagal dihapus !');
          document.location.href = 'index.php';
      </script>
  ";
}

?>