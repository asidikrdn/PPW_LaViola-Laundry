<?php
// Autentifikasi Session
session_start();
if (!isset($_SESSION['login'])) {
  header('Location: ../login');
  exit;
}

require '../functions.php';

$transaksi = query('SELECT tbl_transaksi.id_transaksi, tbl_transaksi.tgl_masuk, tbl_pelanggan.nama_pel, tbl_paket.nama_paket, tbl_paket.multiplier, tbl_layanan.nama_layanan, tbl_layanan.harga, tbl_transaksi.qty, tbl_status.keterangan, tbl_petugas.nama_petugas 
FROM tbl_transaksi, tbl_pelanggan, tbl_paket, tbl_layanan, tbl_status, tbl_petugas 
WHERE tbl_pelanggan.id_pel = tbl_transaksi.id_pel AND tbl_layanan.id_layanan = tbl_transaksi.id_layanan AND tbl_paket.id_paket = tbl_transaksi.id_paket AND tbl_status.id_status = tbl_transaksi.id_status AND tbl_petugas.id_petugas = tbl_transaksi.id_petugas
ORDER BY tbl_status.id_status, tbl_paket.prioritas, tbl_transaksi.tgl_masuk ASC');

$userlogin = $_SESSION['login'];
$isUserAdmin = query("SELECT * FROM tbl_petugas WHERE tbl_petugas.id_petugas = '$userlogin'");

// Mengaktifkan class actiive pada sidebar
$transaksiNavStatus = "active";
?>
<!DOCTYPE html>
<html lang="en">
<!-- Head -->
<?php include '../partial/head.php'; ?>

<body>
  <div class="container-fluid">
    <!-- Sidebar Web Version-->
    <?php include '../partial/sidebar.php'; ?>
    <!-- Header -->
    <?php include '../partial/header.php'; ?>
    <div class="row">
      <div class="col-md-10 offset-md-2 p-0">
        <div class="container-fluid p-0">
          <!-- Main Content -->
          <main id="kontenUtama">
            <div class="row g-0">
              <div class="col-12">
                <div class="px-2 d-flex flex-row justify-content-between bg-light">
                  <h4>Transaksi</h4>
                  <!-- <h4>Tanggal : xx - xx - xxxx</h4> -->
                </div>
              </div>
            </div>

            <!-- Tabel Transaksi -->
            <div class="row g-0">
              <div class="col-12 p-3" style="overflow-x: scroll;">
                <a href="tambah_transaksi.php" class="btn btn-primary ms-0 my-2"><i class="bi bi-plus-circle"></i> Transaksi</a>
                <table class="mx-auto table bg-white">
                  <tr class="table-danger text-center">
                    <th>No</th>
                    <th>Tanggal Transaksi</th>
                    <th>Pelanggan</th>
                    <!-- <th>Paket</th> -->
                    <th>Keterangan Order</th>
                    <th>Qty (kg / pcs)</th>
                    <th>Total Harga</th>
                    <th>Terakhir Diproses Oleh</th>
                    <th>Status Order</th>
                    <th <?php if ($isUserAdmin[0]['role'] == 'admin') { ?>colspan="2" <?php }; ?>>Aksi</th>
                  </tr>
                  <?php foreach ($transaksi as $trx) : ?>
                    <?php $i = 1;
                    $harga = $trx['multiplier'] * $trx['harga'] * $trx['qty']; ?>
                    <tr class="text-center">
                      <td><?= $i; ?></td>
                      <td><?= $trx['tgl_masuk']; ?></td>
                      <td><?= $trx['nama_pel']; ?></td>
                      <!-- <td><?= $trx['nama_paket']; ?></td> -->
                      <td>[<?= $trx['nama_paket']; ?>] <?= $trx['nama_layanan']; ?></td>
                      <td><?= $trx['qty']; ?></td>
                      <td>
                        <?= rupiah($harga); ?>
                        <!-- <?= rupiah($trx['total_harga']); ?> -->
                      </td>
                      <td><?= $trx['nama_petugas']; ?></td>
                      <td><?= $trx['keterangan']; ?></td>
                      <td>
                        <?php if ($trx['keterangan'] == "Baru") { ?>
                          <a href="update_status.php?id_trx=<?php echo $trx['id_transaksi']; ?>" class="btn btn-warning">Proses</a>
                          <!-- <button class="btn btn-warning">Proses</button> -->
                        <?php } ?>
                        <?php if ($trx['keterangan'] == "Dalam Proses") { ?>
                          <a href="update_status.php?id_trx=<?php echo $trx['id_transaksi']; ?>" class="btn btn-success">Selesaikan</a>
                          <!-- <button class="btn btn-success">Selesaikan</button> -->
                        <?php } ?>
                        <?php if ($trx['keterangan'] == "Selesai") { ?>
                          <a href="update_status.php?id_trx=<?php echo $trx['id_transaksi']; ?>" class="btn btn-info">Diambil</a>
                          <!-- <button class="btn btn-info">Diambil</button> -->
                        <?php } ?>
                        <a href="cetak_invoice.php?id_trx=<?php echo $trx['id_transaksi']; ?>" target="_blank" class="btn btn-secondary"><i class="bi bi-printer"></i></a>
                      </td>
                      <?php if ($isUserAdmin[0]['role'] == 'admin') { ?>
                        <td>
                          <a href="hapus_transaksi.php?id_trx=<?php echo $trx['id_transaksi']; ?>" class="btn btn-danger" onclick="return confirm('Yakin?');"><i class="bi bi-trash"></i></a>
                        </td>
                      <?php }; ?>
                    </tr>
                  <?php $i++;
                  endforeach; ?>
                </table>
              </div>
            </div>

          </main>
        </div>
      </div>
    </div>
  </div>
</body>
<script src="../assets/js/bootstrap.bundle.min.js"></script>

</html>