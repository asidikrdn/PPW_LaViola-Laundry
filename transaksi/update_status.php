<?php
// Autentifikasi Session
session_start();
if (!isset($_SESSION['login'])) {
  header('Location: ../login');
  exit;
}

require '../functions.php';

// Menangkap id_transaksi 
$id_transaksi = $_GET['id_trx'];

// Menjalankan fungsiupdate
if (update_transaksi($id_transaksi) > 0){
  echo "
      <script>
          alert('Status berhasil diupdate !');
          document.location.href = 'index.php';
      </script>
  ";
} else {
  echo "
      <script>
          alert('Status gagal diupdate !');
          document.location.href = 'index.php';
      </script>
  ";
}

?>