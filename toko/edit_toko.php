<?php
// Autentifikasi Session
session_start();
if (!isset($_SESSION['login'])) {
  header('Location: ../login');
  exit;
}


require '../functions.php';

if (isset($_POST['update_toko'])) {
  if (update_toko($_POST) > 0) {
    echo "
                <script>
                    alert('Data Toko berhasil diubah');
                </script>
            ";
    header('Location: index.php');
  } else {
    echo mysqli_error($conn);
  }
}

$nama_toko = $_GET["nama_toko"];
$toko = query("SELECT * FROM tbl_id_toko WHERE nama_toko = '$nama_toko'");
?>

<!DOCTYPE html>
<html lang="en">
<!-- Head -->
<?php include '../partial/head.php'; ?>

<body>
  <div class="container-fluid">
    <!-- Sidebar Web Version-->
    <?php include '../partial/sidebar.php'; ?>
    <!-- Header -->
    <?php include '../partial/header.php'; ?>
    <div class="row">
      <div class="col-md-10 offset-md-2 p-0">
        <div class="container-fluid p-0">
          <!-- Main Content -->
          <main id="kontenUtama">
            <div class="row g-0">
              <div class="col-12">
                <div class="px-2 d-flex flex-row justify-content-between bg-light">
                  <h4>Form Ubah Data Toko</h4>
                  <!-- <h4>Tanggal : xx - xx - xxxx</h4> -->
                  <a href="index.php" class="btn btn-secondary my-1">Kembali</a>
                </div>
              </div>
            </div>

            <!-- Form -->
            <div class="row g-0">
              <div class="col-12 p-3">
                <div class="container-fluid">
                  <div class="d-flex flex-row justify-content-center align-items-center" style="height: 100vh;">
                    <div class="div">
                      <form action="" method="post">
                        <label for="nama_toko">Nama Toko :</label>
                        <input type="text" name="nama_toko" id="nama_toko" maxlength="30" required class="form-control" value="<?php echo $toko[0]['nama_toko']; ?>">
                        <br>
                        <label for="telp_toko">No Telepon :</label>
                        <input type="text" name="telp_toko" id="telp_toko" maxlength="30" required class="form-control" value="<?php echo $toko[0]['telp_toko']; ?>">
                        <br>
                        <label for="alamat_toko">Alamat :</label>
                        <input type="text" name="alamat_toko" id="alamat_toko" maxlength="30" required class="form-control" value="<?php echo $toko[0]['alamat_toko']; ?>">
                        <br>
                        <button type="submit" name="update_toko" id="update_toko" class="btn btn-primary">Update Data Toko</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </main>
        </div>
      </div>
    </div>
  </div>
</body>
<script src="../assets/js/bootstrap.bundle.min.js"></script>

</html>