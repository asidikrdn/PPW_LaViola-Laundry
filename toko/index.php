<?php
// Autentifikasi Session
session_start();
if (!isset($_SESSION['login'])) {
  header('Location: ../login');
  exit;
}

require '../functions.php';

$toko = query("SELECT * FROM tbl_id_toko");

$userlogin = $_SESSION['login'];
$isUserAdmin = query("SELECT * FROM tbl_petugas WHERE tbl_petugas.id_petugas = '$userlogin'");

?>
<!DOCTYPE html>
<html lang="en">
<!-- Head -->
<?php include '../partial/head.php'; ?>

<body>
  <div class="container-fluid">
    <!-- Sidebar Web Version-->
    <?php include '../partial/sidebar.php'; ?>
    <!-- Header -->
    <?php include '../partial/header.php'; ?>
    <div class="row">
      <div class="col-md-10 offset-md-2 p-0">
        <div class="container-fluid p-0">
          <!-- Main Content -->
          <main id="kontenUtama">
            <div class="row g-0">
              <div class="col-12">
                <div class="px-2 d-flex flex-row justify-content-between bg-light">
                  <h4>Detail Toko</h4>
                  <!-- <h4>Tanggal : xx - xx - xxxx</h4> -->
                </div>
              </div>
            </div>

            <!-- Tabel Transaksi -->
            <div class="row g-0">
              <div class="col-12 col-md-2 offset-md-3 p-3">
                <div class="d-flex flex-row justify-content-center align-items-center" style="height: 100vh;">
                  <!-- Logo -->
                  <img src="../assets/img/LaViola_transparent.png" alt="" width="250px">
                </div>
              </div>
              <div class="col-12 col-md-4 p-3">
                <div class="d-flex flex-row justify-content-center align-items-center" style="height: 100vh; font-size: 1.5em;">
                  <!-- Detail Toko -->
                  <table>
                    <tr>
                      <th width="150px">Nama Toko</th>
                      <td width="20px">:</td>
                      <td><?=$toko[0]['nama_toko'];?></td>
                    </tr>
                    <tr>
                      <th>No Telepon</th>
                      <td>:</td>
                      <td><?=$toko[0]['telp_toko'];?></td>
                    </tr>
                    <tr>
                      <th>Alamat</th>
                      <td>:</td>
                      <td><?=$toko[0]['alamat_toko'];?></td>
                    </tr>
                    <tr>
                      <?php if ($isUserAdmin[0]['role'] == 'admin') { ?>
                        <td colspan="3" class="text-center">
                          <a href="edit_toko.php?nama_toko=<?php echo $toko[0]['nama_toko']; ?>" class="btn btn-warning"><i class="bi bi-pencil"> Detail Toko</i></a>
                        </td>
                      <?php }; ?>
                    </tr>
                  </table>
                </div>
              </div>
            </div>

          </main>
        </div>
      </div>
    </div>
  </div>
</body>
<script src="../assets/js/bootstrap.bundle.min.js"></script>

</html>