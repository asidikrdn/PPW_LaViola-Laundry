<?php
session_start();

// Jika sudah login, maka tidak bisa membuka halaman login
if (isset($_SESSION['login'])) {
  header('Location: ../dashboard/');
  exit;
}

// Memanggil file function
require '../functions.php';

// cek tombol login
if (isset($_POST['login'])) {

  // menangkap data username dan password
  $username = $_POST['username'];
  $password = $_POST['password'];
  // var_dump($username);

  // cek username
  $cek_username = mysqli_query($conn, "SELECT * FROM tbl_petugas WHERE username='$username'");
  if (mysqli_num_rows($cek_username) == 1) {

    // var_dump($cek_username);
    // cek password
    $row = mysqli_fetch_assoc($cek_username);
    if (password_verify($password, $row['password'])) {

      // Ambil data dari tabel petugas
      $petugas = query("SELECT tbl_petugas.id_petugas 
            FROM tbl_petugas
            WHERE tbl_petugas.username = '$username'");

      foreach ($petugas as $data) :
        // set session
        $_SESSION['login'] = $data['id_petugas'];;
      endforeach;

      header("Location: ../dashboard/");
      exit;
    }
  }

  $error = true;
}

?>

<!DOCTYPE html>
<html lang="en">
<!-- Head -->
<?php include '../partial/head.php'; ?>

<body>
  <div class="container-fluid">
    <div class="d-flex flex-row justify-content-center align-items-center" style="height: 100vh;">
      <div class="div">
        <form action="" method="post">
          <label for="username">Username :</label>
          <input type="text" name="username" id="username" maxlength="10" required class="form-control">
          <br>
          <label for="password">Password :</label>
          <input type="password" name="password" id="password" required class="form-control">
          <br>
          <div class="text-center">
          <button type="submit" name="login" id="login" class="btn btn-primary">Login</button>
          <button type="reset" class="btn btn-danger">Batal</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</body>
<script src="../assets/js/bootstrap.bundle.min.js"></script>

</html>